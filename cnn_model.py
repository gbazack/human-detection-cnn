# Recommendation: Developers are recommended to run these codes on a jupyter notebook
#
# Import libraries
import tensorflow as tf
import numpy as np
from pywt import wavedecn
from scipy.signal import hilbert2
from matplotlib import pyplot as plt
from google.colab import drive

drive.mount('/content/drive')

#training data sets
training_dataset =np.load('/content/drive/MyDrive/Research/iruwbdata/queuing/training.npz')
training_data =training_dataset['data']
training_labels= training_dataset['labels']

#validation data sets
#validation_dataset =np.load('/content/drive/MyDrive/Research/iruwbdata/queuing/validation.npz')
#validation_data =validation_dataset['data']
#validation_labels= validation_dataset['labels']

#test data sets
test_dataset =np.load('/content/drive/MyDrive/Research/iruwbdata/queuing/test.npz')
test_data =test_dataset['data']
test_labels= test_dataset['labels']


def dwt_transform(dataset, _wavelets):
# This function computes a level=2 d-dimensional discrete wavelet transform of
# Input dataset of shape (N,x,y)
#           where N= number of 2d-signals of shape (x,y)
#           Example:
#           >>dataset.shape
#             (4000,20,1280)
#           >>transformed_data= dwt_transform(dataset, 'db4')
#           >>transformed_data.shape
#             (4000,10,365)
     
    data_tmp= []
    for data in dataset:
        coefs= wavedecn(data, _wavelets, level=2)
        data_tmp.append(coefs[0])

    return np.array(data_tmp)


def moving_average(data):
    # This is the implementation of the moving average filter
    # Input: data of shape (n,x,y,z)
    #        n= None or batch size
    #        z= convolution channels, default is 1
    # The filter lowpass_filter follows a normal distribution N(m/x, std/x)
    #        m= statistical mean of data
    #        std= standard deviation of data        
    #        lowpass_filter is of shape (x,y,z)

    # Generation of the lowpass filter # List index: 0,1,2,3 -- (n, x, y, z)
    scale_factor= tf.math.divide(1, data.shape[1])
    scale_factor= tf.cast(scale_factor, tf.float32)
    
    lowpass_filter= tf.random.normal(data.shape[1:], tf.math.reduce_mean(data), tf.math.reduce_std(data), dtype=tf.float32)
    lowpass_filter= tf.multiply(scale_factor, lowpass_filter) 
    
    # Filtering the data
    item_filtered= data - lowpass_filter
    
    return item_filtered


def distance_compensation(data):
    var= 2*23.328e9
    scale_factor= tf.math.pow(tf.math.divide(3e8, var), 1.7)
    sample_size= data.shape[2]
    sample= tf.range(1,sample_size+1, delta=1, dtype=tf.float32)
    sample= tf.expand_dims(sample, axis=-1)
    PL_Ind= tf.math.multiply(sample, scale_factor)

    compensated_data= tf.math.multiply(data, PL_Ind)

    return compensated_data


model= tf.keras.models.Sequential(
    [
     tf.keras.layers.Lambda(lambda x: x/255.0, input_shape=(10,325)),
     tf.keras.layers.Reshape((10,325,1)),
     tf.keras.layers.Conv2D(256, (3,3), strides=(1,1), activation='relu', padding='same', kernel_initializer='glorot_uniform', name='ConvLayer1'),
     tf.keras.layers.Lambda(lambda x: moving_average(x), name='movingAverageLayer', trainable=False),
     #tf.keras.layers.Lambda(lambda x: distance_compensation(x), name='DistcompLayer', trainable=False),
     tf.keras.layers.Flatten(),
     tf.keras.layers.Dense(256, activation='gelu', name='HiddenLayer1'),
     tf.keras.layers.Dropout(0.5),
     tf.keras.layers.Dense(512, activation='gelu', name='HiddenLayer2'),
     tf.keras.layers.Dropout(0.5),
     tf.keras.layers.Dense(512, activation='gelu', name='HiddenLayer3'),
     tf.keras.layers.Dropout(0.5),
     tf.keras.layers.Dense(16, activation='softmax', name='OutputLayer')
     ]
     )

# Define a callback to stop the training
# when 99% of accuracy is reached
class CallBack(tf.keras.callbacks.Callback): 
  def on_train_begin(self, epochs, logs={}):
      if (logs.get('accuracy')>0.99):
        print("\n Stopping the training: Accuracy 99% reached")
        self.model.stop_training= True

# Compute the discrete wavelet decomposition of
# training and test datasets with Daubechies 4 at level=2
training_data= dwt_transform(training_data, 'db4')
#validation_data= dwt_transform(validation_data, 'db4')
test_data= dwt_transform(test_data, 'db4')

Metrics=['accuracy']   #Performance metrics
# Compile the cnn instance
model.compile(loss='sparse_categorical_crossentropy', optimizer=tf.keras.optimizers.Nadam(learning_rate=10e-7, epsilon=10e-7), metrics=Metrics)

history=model.fit(training_data, training_labels, validation_data=(test_data, test_labels), epochs=30, verbose=1, workers=3, use_multiprocessing=True)

model.save("/content/drive/MyDrive/Research/iruwbdata/CNNmhd.h5")
model= tf.keras.models.load_model("/content/drive/MyDrive/Research/iruwbdata/CNNmhd.h5")

acc= history.history['accuracy']
loss= history.history['loss']
val_acc= history.history['val_accuracy']
val_loss= history.history['val_loss']
x_axis =np.arange(len(acc))


plt.plot(x_axis, acc, label="Training accuracy");
#plt.plot(x_axis, loss, label="Training loss");
plt.plot(x_axis, val_acc, label="Validation accuracy");
#plt.plot(x_axis, val_loss, label="Validation loss");
plt.xlabel('Epochs')
plt.ylabel('Performance');
plt.legend(loc="upper right", bbox_to_anchor=[1, 1],ncol=2);
plt.title("Performance of the proposed CNN")
plt.grid(True)
#plt.savefig('/content/drive/MyDrive/Research/iruwbdata/results/wCNNepoch600Acc.png', dpi=150)
plt.show()
