# Recommendation: Developers are recommended to run these codes on a jupyter notebook
#
# Import libraries
import os
import numpy as np

dir_path=os.path.join(os.getcwd(),'rawdata/queuing')
os.chdir(dir_path)
os.getcwd()

def file_to_dataset(filename,dataset):
    # This function allows to extract the content of 
    # a mat (txt) file to a numpy ndarray
    #
    file= open(filename, 'r')
    lines= file.readlines()
    #
    split_size= 10            #split_size: used to split the 200 lines (or signals)
    x_axis= int(len(lines)/split_size)
    y_axis= int((len(lines[0])+1)/10)
    #
    for step in range(split_size):
        data= np.zeros((x_axis, y_axis))
        start= x_axis*step
        end= x_axis*(step+1)
        transform_to_ndarray(lines, data, start, end, step)
        dataset.append(data)

def transform_to_ndarray(lines, data, start, end, step):
    #
    x_axis, y_axis= data.shape
    for index in range(start, end):
        # Remove special caracters
        lines[index].replace('\t', ' ')
        lines[index].replace('\n', 'callbacks=[callback],')
        tmp= []
        #
        #For each row of the data, convert each 10 strings of 1280 to float
        for i in range(0, len(lines[index]), 10):
            tmp.append(lines[index][i:i+10])
        data[index -step*x_axis]= tmp

def increase_dataset(dataset, labels, threshold, multiplier):
    # This function increases the size of the dataset by a factor of the multiplier
    # Input parameters:
    #      dataset: ndarray of shape (x,y,z)
    #      labels: list of x integers
    #      threshold: ndarray of shape (x,y,z)
    # Output:
    #      tmp_dataset: ndarray of shape (multiplier*x,y,z)
    #      tmp_labels: list of multiplier*x integers
    #
    x_axis, y_axis, z_axis= dataset.shape
    tmp_dataset= np.zeros((multiplier*x_axis, y_axis, z_axis))
    tmp_labels= labels.copy()
    #
    for index in range(multiplier-1):
        tmp= dataset - threshold[index]
        tmp_dataset[index*x_axis : (index+1)*x_axis]= tmp
        tmp_labels.extend(labels)
    #
    tmp_dataset[(multiplier -1)*x_axis : (multiplier)*x_axis]= dataset
    return tmp_dataset, tmp_labels

# Save the threshold_dataset
# It will be used to filter the remaining dataset, in order to generate more data
# It contains reflected signals from 0 people
threshold=[]
#List all the files in the directory
listfiles=os.listdir('test/0/.')
for filename in listfiles:
    filename=os.path.join('test/0',filename)
    file_to_dataset(filename, threshold)
threshold_data= np.array(threshold)

dataset=[]; labels=[]

# Extract the data from the remaining files in the test/validation repository
listfiles=os.listdir('test/15/.')
for x in range(10*len(listfiles)):
    labels.append(15)
    
for filename in listfiles:
    filename=os.path.join('test/15',filename)
    file_to_dataset(filename, dataset)


# Apply threshold to the dataset and
# Add thresholded data to the dataset
test_dataset, test_labels= increase_dataset(dataset, labels, threshold, 10)
np.savez('test', data=np.array(test_dataset), labels=np.array(test_labels))

